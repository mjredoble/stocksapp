# StocksApp

StocksApp is an iOS Application that gives you information about the latest gainers and losers in the Stock Market using Alpha Vantage API. You can save your desired stocks on the Watchlist from the movers list.

## Library Used
1. SwiftUI

## Test
1. Unit Test

## Caching Mechanism Used
1. URLCache

I used the following caching mechanism as my data storage because it is what I using in my projects to cater offline data. URLCache by default is already saving all the response we receive and it is just kept inside our iPhone, by using a correct URLSessionConfiguration your urlsession will give the last saved data requested when you still have active connections.

 It is also handy to use urlcache with Etag.

```Swift
let configuration = URLSessionConfiguration.default
configuration.httpCookieAcceptPolicy = .always
configuration.urlCache?.memoryCapacity = 50_000_000
configuration.urlCache?.diskCapacity = 100_000_000
return configuration
```

## Stocks API Used
1. Alpha Vantage (FREE)


