import Foundation

struct APIClientResult {
    let statusCode: Int
    let headers: [AnyHashable: Any]
    let body: Data
}

enum APIClientError: Error {
    case applicationLevel(statusCode: Int, body: Data?)
    case lowerLevel(_ error: Error)
}

enum APIRequestStatus {
    case inprogress
    case done
}

enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
}

final class APIClient {
    func request(url: URL, method: HTTPMethod, body: Data? = nil) async throws -> APIClientResult {
        var request = URLRequest(url: url)
        request.timeoutInterval = TimeInterval(0)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = body
        
        let session = URLSession(configuration: getConfiguration())
        
        do {
            let (data, urlResponse) = try await session.data(for: request)
            
            let response = urlResponse as! HTTPURLResponse
            let str = String(decoding: data, as: UTF8.self)
            
            switch response.statusCode {
            case  200...204:
                print("🟢 API: \(url.absoluteString) --- DATA:\(data) ---STATUS:\(response.statusCode)")
                return APIClientResult(statusCode: response.statusCode, headers: response.allHeaderFields, body: data)
            default:
                print("🔴 API: \(url.absoluteString) --- DATA:\(str) ---STATUS:\(response.statusCode)")
                throw APIClientError.applicationLevel(statusCode: response.statusCode, body: data)
            }
        } catch {
            throw APIClientError.lowerLevel(error)
        }
    }
    
    private func getConfiguration() -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.httpCookieAcceptPolicy = .always
        configuration.urlCache?.memoryCapacity = 50_000_000
        configuration.urlCache?.diskCapacity = 100_000_000
        return configuration
    }
}
