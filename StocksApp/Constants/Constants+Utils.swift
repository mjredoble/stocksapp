import Foundation

struct ApiKeys {
    static let alphaVantage = "XUB2I4SBOSST8KBK"
    static let alphaVantage2 = "2QLF8IHFLR0N9GS8"
}

struct BaseURLs {
    static let url = "https://www.alphavantage.co/"
}

enum ServiceError: Error {
    case badUrl
    case badResponse
}

enum QueryType: String {
    case topGainersLosers = "TOP_GAINERS_LOSERS"
    case overview = "OVERVIEW"
 }

struct UrlUtils {
    static func topGainersLosersURL() -> URLComponents {
        let baseURL = BaseURLs.url + "query?"
        var components = URLComponents(string: baseURL)
        components!.queryItems = [
            URLQueryItem(name: "function", value: QueryType.topGainersLosers.rawValue),
            URLQueryItem(name: "apikey", value: ApiKeys.alphaVantage),
        ]
        
        return components!
    }
    
    static func stockDetailURL(symbol: String) -> URLComponents {
        let baseURL = BaseURLs.url + "query?"
        var components = URLComponents(string: baseURL)
        components!.queryItems = [
            URLQueryItem(name: "function", value: QueryType.overview.rawValue),
            URLQueryItem(name: "symbol", value: symbol),
            URLQueryItem(name: "apikey", value: ApiKeys.alphaVantage2),
        ]
        
        return components!
    }
}
