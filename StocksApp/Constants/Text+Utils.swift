import Foundation
import SwiftUI

struct TextFormatting {
    
    @ViewBuilder
    static func changeText(text: String) -> some View {
        let value = Double(text.replacingOccurrences(of: "%", with: ""))
        Text("(\(text))")
            .font(.title3)
            .fontWeight(.medium)
            .textCase(.uppercase)
            .foregroundStyle(value ?? 1 < 0 ? .red : .green)
    }
}
