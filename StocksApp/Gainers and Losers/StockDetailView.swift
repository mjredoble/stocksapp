import SwiftUI

struct StockDetailView: View {
    @ObservedObject var viewModel: TopGainersLosersViewModel
    @State var stockInfo: StockInfo
    @State var selectedStockOverview: StockOverviewDTO?
    
    @State private var showError = false
    @State private var alertMessage = ""
    
    @ViewBuilder
    var addToWachlistButton: some View {
        Button("Add To Watchlist") {
            do {
                try viewModel.addToWatchlist(stock: stockInfo)
                alertMessage = "Stock added to watchlist: \(stockInfo.ticker)"
            } catch {
                if error as! WatchlistError == WatchlistError.alreadyAdded {
                    alertMessage = "This is already in your watchlist"
                } else {
                    alertMessage = "An error occured: \(error)"
                }
            }
            
            showError.toggle()
        }
    }

    @ViewBuilder
    var removeToWachlistButton: some View {
        Button("Remove To Watchlist") {
            viewModel.removeFromWatchlist(stock: stockInfo)
            alertMessage = "Stock is removed!"
            showError.toggle()
        }
    }
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(alignment: .center, spacing: 0) {
                    TopGainersLosersListCellView(stock: stockInfo)
                    if viewModel.status == .inprogress {
                        ProgressView()
                            .padding()
                    } else {
                        Section {
                            LabeledContent("Company Name:", value: selectedStockOverview?.name ?? "")
                            LabeledContent("Description:", value: selectedStockOverview?.description ?? "")
                            LabeledContent("Address:", value: selectedStockOverview?.address ?? "")
                            LabeledContent("Country:", value: selectedStockOverview?.country ?? "")
                            LabeledContent("Exchange:", value: selectedStockOverview?.exchange ?? "")
                            LabeledContent("Currency:", value: selectedStockOverview?.currency ?? "")
                            LabeledContent("EBITDA:", value: selectedStockOverview?.ebitda ?? "")
                        }
                        .padding()
                    }
                }
            }
        }
        .task {
            do {
                self.selectedStockOverview = try await TopGainersLosersViewModel.getDummyStockDTO()
            } catch {
                print(error)
            }
        }
        .navigationTitle(stockInfo.ticker)
        .navigationBarHidden(false)
        .toolbar {
            if viewModel.isAlreadySaved(stock: stockInfo) {
                removeToWachlistButton
            } else {
                addToWachlistButton
            }
        }
        .alert(alertMessage, isPresented: $showError) {
            Button("OK", role: .cancel) {
                showError = false
            }
        }
    }
}

#Preview {
    StockDetailView(viewModel: TopGainersLosersViewModel(),
                    stockInfo: StockInfo(ticker: "CEADW",
                                         price: "0.01",
                                         changeAmount: "0.0062",
                                         changePercentage: "163.1579%",
                                         volume: "26000"))
}
