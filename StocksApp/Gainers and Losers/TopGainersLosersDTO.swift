import Foundation

struct TopGainersLosersDTO: Codable {
    let metadata: String
    let lastUpdated: String
    let topGainers: [StockInfo]
    let topLosers: [StockInfo]
    let mostActivelyTraded: [StockInfo]
    
    enum CodingKeys: String, CodingKey {
        case metadata
        case lastUpdated = "last_updated"
        case topGainers = "top_gainers"
        case topLosers = "top_losers"
        case mostActivelyTraded = "most_actively_traded"
    }
}

struct StockInfo: Codable {
    let id = UUID()
    let ticker: String
    let price: String
    let changeAmount: String
    let changePercentage: String
    let volume: String
    
    enum CodingKeys: String, CodingKey {
        case ticker
        case price
        case changeAmount = "change_amount"
        case changePercentage = "change_percentage"
        case volume
    }
}

struct StockOverviewDTO: Codable {
    let symbol: String
    let assetType: String
    let name: String
    let description: String
    let cik: String
    let exchange: String
    let currency: String
    let country: String
    let sector: String
    let industry: String
    let address: String
    let fiscalYearEnd: String
    let latestQuarter: String
    let marketCapitalization: String
    let ebitda: String
    let peRatio: String
    let pegRatio: String
    let bookValue: String
    let dividendPerShare: String
    let dividendYield: String
    let eps: String
    let revenuePerShareTTM: String
    let profitMargin: String
    let operatingMarginTTM: String
    let returnOnAssetsTTM: String
    let returnOnEquityTTM: String
    let revenueTTM: String
    let grossProfitTTM: String
    let dilutedEPSTTM: String
    let quarterlyEarningsGrowthYOY: String
    let quarterlyRevenueGrowthYOY: String
    let analystTargetPrice: String
    let trailingPE: String
    let forwardPE: String
    let priceToSalesRatioTTM: String
    let priceToBookRatio: String
    let evToRevenue: String
    let evToEBITDA: String
    let beta: String
    let week52High: String
    let week52Low: String
    let day50MovingAverage: String
    let day200MovingAverage: String
    let sharesOutstanding: String
    let dividendDate: String
    let exDividendDate: String
    
    enum CodingKeys: String, CodingKey {
        case symbol = "Symbol"
        case assetType = "AssetType"
        case name = "Name"
        case description = "Description"
        case cik = "CIK"
        case exchange = "Exchange"
        case currency = "Currency"
        case country = "Country"
        case sector = "Sector"
        case industry = "Industry"
        case address = "Address"
        case fiscalYearEnd = "FiscalYearEnd"
        case latestQuarter = "LatestQuarter"
        case marketCapitalization = "MarketCapitalization"
        case ebitda = "EBITDA"
        case peRatio = "PERatio"
        case pegRatio = "PEGRatio"
        case bookValue = "BookValue"
        case dividendPerShare = "DividendPerShare"
        case dividendYield = "DividendYield"
        case eps = "EPS"
        case revenuePerShareTTM = "RevenuePerShareTTM"
        case profitMargin = "ProfitMargin"
        case operatingMarginTTM = "OperatingMarginTTM"
        case returnOnAssetsTTM = "ReturnOnAssetsTTM"
        case returnOnEquityTTM = "ReturnOnEquityTTM"
        case revenueTTM = "RevenueTTM"
        case grossProfitTTM = "GrossProfitTTM"
        case dilutedEPSTTM = "DilutedEPSTTM"
        case quarterlyEarningsGrowthYOY = "QuarterlyEarningsGrowthYOY"
        case quarterlyRevenueGrowthYOY = "QuarterlyRevenueGrowthYOY"
        case analystTargetPrice = "AnalystTargetPrice"
        case trailingPE = "TrailingPE"
        case forwardPE = "ForwardPE"
        case priceToSalesRatioTTM = "PriceToSalesRatioTTM"
        case priceToBookRatio = "PriceToBookRatio"
        case evToRevenue = "EVToRevenue"
        case evToEBITDA = "EVToEBITDA"
        case beta = "Beta"
        case week52High = "52WeekHigh"
        case week52Low = "52WeekLow"
        case day50MovingAverage = "50DayMovingAverage"
        case day200MovingAverage = "200DayMovingAverage"
        case sharesOutstanding = "SharesOutstanding"
        case dividendDate = "DividendDate"
        case exDividendDate = "ExDividendDate"
    }
}

enum Movers: Int {
    case gainers = 0
    case losers = 1
    case activelyTraded = 2
}
