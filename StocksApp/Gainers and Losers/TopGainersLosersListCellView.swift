import SwiftUI

struct TopGainersLosersListCellView: View {
    @State var stock: StockInfo

    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 0) {
                Text(stock.ticker)
                    .font(.title3)
                    .fontWeight(.bold)
                    .textCase(.uppercase)
                    .foregroundStyle(.black)
                HStack {
                    Text("Volume:")
                        .font(.caption)
                        .fontWeight(.light)
                        .textCase(.uppercase)
                        .foregroundStyle(.gray)
                    
                    Text(stock.volume)
                        .font(.caption2)
                        .fontWeight(.light)
                        .textCase(.uppercase)
                        .foregroundStyle(.gray)
                }
            }
            
            Spacer()
    
            Text(stock.price)
                .font(.title2)
                .fontWeight(.medium)
                .textCase(.uppercase)
                .foregroundStyle(.black)

            Spacer()
            
            VStack(alignment: .trailing, spacing: 0) {
                TextFormatting.changeText(text: stock.changeAmount)
                TextFormatting.changeText(text: stock.changePercentage)
            }
        }
        .padding()
        
        Divider()
    }
}

#Preview {
    TopGainersLosersListCellView(stock: StockInfo(ticker: "CEADW",
                                                  price: "0.01",
                                                  changeAmount: "0.0062",
                                                  changePercentage: "163.1579%",
                                                  volume: "26000"))
}
