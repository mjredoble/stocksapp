import SwiftUI

struct TopGainersLosersListView: View {
    @ObservedObject var viewModel: TopGainersLosersViewModel
    @Binding var selectedView: Movers.RawValue
    
    var body: some View {
        ForEach(viewModel.getStockList(type: Movers(rawValue: selectedView)!),  id: \.id) { stock in
            NavigationLink(destination: StockDetailView(viewModel: viewModel, stockInfo: stock), label: {
                TopGainersLosersListCellView(stock: stock)
            }).tint(.gray)
        }
    }
}

#Preview {
    TopGainersLosersListView(viewModel: TopGainersLosersViewModel(), selectedView: .constant(0))
}
