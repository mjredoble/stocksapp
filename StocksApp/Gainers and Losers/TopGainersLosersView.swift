import SwiftUI

struct TopGainersLosersView: View {
    @ObservedObject var viewModel: TopGainersLosersViewModel
    @State private var selectedView = 0
    
    var body: some View {
        NavigationStack {
            ScrollView {
                Picker("Select movers", selection: $selectedView) {
                    Text("Gainers").tag(0)
                    Text("Losers").tag(1)
                }
                .pickerStyle(.segmented)
                
                VStack(spacing: 0) {
                    if viewModel.status == .inprogress {
                        ProgressView()
                            .padding()
                    } else {
                        TopGainersLosersListView(viewModel: viewModel, selectedView: $selectedView)
                    }
                }
            }
            .navigationTitle("Movers")
            .task {
                do {
                    try await viewModel.getLocalMovers()
                } catch {
                    print("ERROR")
                }
            }
        }
    }
}

#Preview {
    TopGainersLosersView(viewModel: TopGainersLosersViewModel())
}
