import SwiftUI

@MainActor
final class TopGainersLosersViewModel: ObservableObject {
    @Published var status = APIRequestStatus.done
    @Published var gainers: [StockInfo] = []
    @Published var losers: [StockInfo] = []
    @Published var activelyTraded: [StockInfo] = []
    @Published var watchlist: [StockInfo] = []
    
    private var storageKey = "watchlistKey"
    
    func getStockList(type: Movers) -> [StockInfo] {
        switch type {
        case .gainers:
            return gainers
        case .losers:
            return losers
        case .activelyTraded:
            return activelyTraded
        }
    }
    
    func getTodaysMovers() async throws {
        let components = UrlUtils.topGainersLosersURL()
        guard let url = components.url else {
            throw ServiceError.badUrl
        }
        
        status = .inprogress
        
        do {
            let result = try await APIClient().request(url: url, method: .GET)
            let moversDTO = try JSONDecoder().decode(TopGainersLosersDTO.self, from: result.body)
            self.gainers = moversDTO.topGainers
            self.losers = moversDTO.topLosers
            self.activelyTraded = moversDTO.mostActivelyTraded
            
            status = .done
        } catch {
            status = .done
            throw ServiceError.badResponse
        }
    }
    
    func getStockOverview(symbol: String) async throws -> StockOverviewDTO {
        let components = UrlUtils.stockDetailURL(symbol: symbol)
        guard let url = components.url else {
            throw ServiceError.badUrl
        }
        
        status = .inprogress
        
        do {
            let result = try await APIClient().request(url: url, method: .GET)
            let overview = try JSONDecoder().decode(StockOverviewDTO.self, from: result.body)
           
            status = .done
            return overview
            
        } catch {
            print(error)
            status = .done
            throw ServiceError.badResponse
        }
    }
    
    func getLocalStock() async throws -> StockOverviewDTO {
        status = .inprogress
        
        do {
            if let fileURL = Bundle.main.url(forResource: "ibm", withExtension: "json") {
                let jsonData = try Data(contentsOf: fileURL)
                let overview = try JSONDecoder().decode(StockOverviewDTO.self, from: jsonData)
                
                status = .done
                return overview
            } else {
                return try await TopGainersLosersViewModel.getDummyStockDTO()
            }
        } catch {
            print(error)
            status = .done
            throw ServiceError.badResponse
        }
    }
    
    func getLocalMovers() async throws {
        status = .inprogress
        
        do {
            if let fileURL = Bundle.main.url(forResource: "movers", withExtension: "json") {
                let jsonData = try Data(contentsOf: fileURL)
                let moversDTO = try JSONDecoder().decode(TopGainersLosersDTO.self, from: jsonData)
                self.gainers.append(contentsOf: moversDTO.topGainers)
                self.losers.append(contentsOf: moversDTO.topLosers)
                self.activelyTraded.append(contentsOf: moversDTO.mostActivelyTraded)
                
                status = .done
            }
        } catch {
            print(error)
            status = .done
            throw ServiceError.badResponse
        }
    }
    
    func isAlreadySaved(stock: StockInfo) -> Bool {
        watchlist.contains(where: { $0.ticker == stock.ticker })
    }
    
    func addToWatchlist(stock: StockInfo) throws {
        if watchlist.count >= 5 {
            throw WatchlistError.full
        }
        
        if isAlreadySaved(stock: stock) {
            throw WatchlistError.alreadyAdded
        }
        
        watchlist.append(stock)
        saveWatchlistToUserDefaults(watchlist)
        getWatchlistFromUserDefaults()
    }
    
    // MARK: - User Defaults for Watchlist
    func getWatchlistFromUserDefaults() {
        if let data = UserDefaults.standard.data(forKey: storageKey) {
            do {
                let decoder = JSONDecoder()
                let cachedlist = try decoder.decode([StockInfo].self, from: data)
                self.watchlist.removeAll()
                self.watchlist.append(contentsOf: cachedlist)
            } catch {
                print("Error decoding array: \(error.localizedDescription)")
            }
        }
    }
    
    func removeFromWatchlist(stock: StockInfo) {
        if let index = watchlist.firstIndex(where: {$0.ticker == stock.ticker}) {
            watchlist.remove(at: index)
            saveWatchlistToUserDefaults(watchlist)
            getWatchlistFromUserDefaults()
        }
    }
    
    private func saveWatchlistToUserDefaults(_ array: [StockInfo]) {
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(array)
        
            UserDefaults.standard.set(data, forKey: storageKey)
        } catch {
            print("Error encoding array: \(error.localizedDescription)")
        }
    }
    
   // MARK: - Dummy Data for Preview
    
    static func getDummyStockDTO() async throws -> StockOverviewDTO {
        if let fileURL = Bundle.main.url(forResource: "ceadw", withExtension: "json") {
            do {
                let jsonData = try Data(contentsOf: fileURL)
                let dummy = try JSONDecoder().decode(StockOverviewDTO.self, from: jsonData)
                return dummy
            } catch {
                print(error)
                throw error
            }
        } else {
            return StockOverviewDTO(symbol: "", assetType: "", name: "", description: "", cik: "", exchange: "", currency: "", country: "", sector: "", industry: "", address: "", fiscalYearEnd: "", latestQuarter: "", marketCapitalization: "", ebitda: "", peRatio: "", pegRatio: "", bookValue: "", dividendPerShare: "", dividendYield: "", eps: "", revenuePerShareTTM: "", profitMargin: "", operatingMarginTTM: "", returnOnAssetsTTM: "", returnOnEquityTTM: "", revenueTTM: "", grossProfitTTM: "", dilutedEPSTTM: "", quarterlyEarningsGrowthYOY: "", quarterlyRevenueGrowthYOY: "", analystTargetPrice: "", trailingPE: "", forwardPE: "", priceToSalesRatioTTM: "", priceToBookRatio: "", evToRevenue: "", evToEBITDA: "", beta: "", week52High: "", week52Low: "", day50MovingAverage: "", day200MovingAverage: "", sharesOutstanding: "", dividendDate: "", exDividendDate: "")
        }
    }
}

enum WatchlistError: Error {
    case full
    case invalidParsing
    case alreadyAdded
}
