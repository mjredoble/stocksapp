import SwiftUI

struct MainView: View {
    @StateObject private var viewModel = TopGainersLosersViewModel()
    
    private var bgColor = UIColor(red: 209.0/255.0, green: 234.0/255.0, blue: 241.0/255.0, alpha: 1.0)
    private var textColor = UIColor(red: 27.0/255.0, green: 42.0/255.0, blue: 68.0/255.0, alpha: 1.0)
    
    init() {
        let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithOpaqueBackground()
            navigationBarAppearance.largeTitleTextAttributes = [.foregroundColor: textColor]
            navigationBarAppearance.titleTextAttributes = [.foregroundColor: textColor]
            navigationBarAppearance.backgroundColor = bgColor
        
            UINavigationBar.appearance().standardAppearance = navigationBarAppearance
            UINavigationBar.appearance().compactAppearance = navigationBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearance
            UITabBar.appearance().barTintColor = bgColor
    }
    
    var body: some View {
        TabView {
            TopGainersLosersView(viewModel: viewModel)
                .tabItem {
                    Image(systemName: "chart.line.uptrend.xyaxis")
                    Text("Gainers And Losers")
                }
            
            WatchlistView(viewModel: viewModel)
                .tabItem {
                    Image(systemName: "eyes.inverse")
                    Text("Watchlist")
                }
        }
    }
}

#Preview {
    MainView()
}
