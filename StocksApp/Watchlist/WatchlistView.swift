import SwiftUI

struct WatchlistView: View {
    @ObservedObject var viewModel: TopGainersLosersViewModel
    
    var body: some View {
        NavigationStack {
            ScrollView {
                VStack(spacing: 0) {
                    if viewModel.status == .inprogress {
                        ProgressView()
                            .padding()
                    } else {
                        if viewModel.watchlist.count > 0 {
                            ForEach(viewModel.watchlist,  id: \.id) { stock in
                                NavigationLink(destination: StockDetailView(viewModel: viewModel, stockInfo: stock), label: {
                                    TopGainersLosersListCellView(stock: stock)
                                }).tint(.gray)
                            }
                        } else {
                            Text("No items in your watchlist")
                                .padding(50)
                        }
                    }
                }
            }
            .navigationTitle("Watchlist")
            .onAppear() {
                viewModel.getWatchlistFromUserDefaults()
            }
        }
    }
}

#Preview {
    WatchlistView(viewModel: TopGainersLosersViewModel())
}
