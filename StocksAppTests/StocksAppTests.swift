import XCTest
@testable import StocksApp

final class StocksAppTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    @MainActor func testAddToWatchlist() {
        let vm = TopGainersLosersViewModel()
        let stock = StockInfo(ticker: "AAPL", price: "1.0", changeAmount: "1.0", changePercentage: "1%", volume: "1")
        try? vm.addToWatchlist(stock: stock)
        XCTAssertTrue(vm.isAlreadySaved(stock: stock))
    }
    
    @MainActor func testRemoveFromWatchlist() {
        let vm = TopGainersLosersViewModel()
        let stock = StockInfo(ticker: "AAPL", price: "1.0", changeAmount: "1.0", changePercentage: "1%", volume: "1")
        try? vm.addToWatchlist(stock: stock)
        
        vm.removeFromWatchlist(stock: stock)
        XCTAssertFalse(vm.isAlreadySaved(stock: stock))
    }
}
